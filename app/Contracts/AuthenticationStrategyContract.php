<?php
declare(strict_types=1);

namespace App\Contracts;

use Illuminate\Http\Request;

interface AuthenticationStrategyContract
{
    public function login(Request $request);
}
