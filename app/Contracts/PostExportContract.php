<?php
declare(strict_types=1);

namespace App\Contracts;

interface PostExportContract
{
    /**
     * @param array $data
     * @param string|null $driver
     */
    public function export(array $data = [], ?string $driver = null);
}
