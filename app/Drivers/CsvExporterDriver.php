<?php
declare(strict_types=1);

namespace App\Drivers;

use App\Contracts\PostExportContract;

final class CsvExporterDriver implements PostExportContract
{
    /**
     * @var array
     */
    private array $options;

    /**
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    /**
     * @param array $data
     * @param string|null $driver
     * @return string
     */
    public function export(array $data = [], ?string $driver = null): string
    {
        return 'export to csv';
    }
}
