<?php
declare(strict_types=1);

namespace App\Filters;

use Closure;
use Illuminate\Database\Eloquent\Builder;

final class FilterByCreatedAt
{
    public function handle(Builder $query, Closure $next)
    {
        if(request()->has('created_at')) {
            $query->whereDate('created_at', request('created_at'));
        }

        return $next($query);
    }
}
