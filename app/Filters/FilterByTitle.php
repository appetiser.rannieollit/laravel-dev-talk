<?php
declare(strict_types=1);

namespace App\Filters;

use Closure;
use Illuminate\Database\Eloquent\Builder;

final class FilterByTitle
{
    public function handle(Builder $query, Closure $next)
    {
        if(request()->has('title')) {
            $query->where('title', request('title'));
        }

        return $next($query);
    }
}
