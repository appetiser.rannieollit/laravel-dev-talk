<?php

namespace App\Http\Controllers\API;

use App\Contracts\PostExportContract;
use App\Filters\FilterByCreatedAt;
use App\Filters\FilterByTitle;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class PostsController extends Controller
{
    public function __construct(
        public PostExportContract $postExportContract
    ){
    }

    public function export(Request $request)
    {
        return $this->postExportContract->export(
            $request->data ?? [],
            $request->driver ?? 'json'
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return app(Pipeline::class)
            ->send(Post::query())
            ->through([
                FilterByCreatedAt::class,
                FilterByTitle::class
            ])
            ->thenReturn()
            ->simplePaginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
