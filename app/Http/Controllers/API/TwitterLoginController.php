<?php
declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Contracts\AuthenticationStrategyContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

final class TwitterLoginController extends Controller
{
    public function __construct(
        public AuthenticationStrategyContract $strategyContract
    ){
    }

    public function login(Request $request)
    {
        return $this->strategyContract->login($request);
    }
}
