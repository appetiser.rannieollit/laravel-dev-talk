<?php
declare(strict_types=1);

namespace App\Manager;

use Illuminate\Support\Manager;
use App\Contracts\PostExportContract;
use App\Drivers\CsvExporterDriver;
use App\Drivers\JsonExporterDriver;

final class PostExportManager extends Manager implements PostExportContract
{
    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver(): string
    {
        return $this->config->get('export.driver', 'json');
    }

    /**
     * Export posts data in JSON format
     */
    public function createJsonDriver(): JsonExporterDriver
    {
        return new JsonExporterDriver($this->config->get('export.json') ?? []);
    }

    /**
     * Export posts data in CSV format
     */
    public function createCsvDriver(): CsvExporterDriver
    {
        return new CsvExporterDriver($this->config->get('export.csv') ?? []);
    }

    /**
     * @param array $data
     * @param string|null $driver
     * @return mixed
     */
    public function export(array $data = [], ?string $driver = null)
    {
        return $this->driver($driver)->export($data);
    }
}
