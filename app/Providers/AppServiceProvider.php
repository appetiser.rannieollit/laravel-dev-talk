<?php

namespace App\Providers;

use App\Contracts\AuthenticationStrategyContract;
use App\Http\Controllers\API\FacebookLoginController;
use App\Http\Controllers\API\TwitterLoginController;
use App\Strategies\Authentication\FacebookAuthentication;
use App\Strategies\Authentication\TwitterAuthentication;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(PostServiceProvider::class);

        // strategies
        $this->app->when(FacebookLoginController::class)
            ->needs(AuthenticationStrategyContract::class)
            ->give(FacebookAuthentication::class);

        $this->app->when(TwitterLoginController::class)
            ->needs(AuthenticationStrategyContract::class)
            ->give(TwitterAuthentication::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
