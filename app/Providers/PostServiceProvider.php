<?php

namespace App\Providers;

use App\Contracts\PostExportContract;
use App\Manager\PostExportManager;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class PostServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PostExportContract::class, function ($app) {
            return new PostExportManager($app);
        });

        $this->app->singleton('export', function ($app) {
            return new PostExportManager($app);
        });

        $this->app->singleton('export.driver', function ($app) {
            return $app['export']->driver();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return ['export', 'export.driver'];
    }
}
