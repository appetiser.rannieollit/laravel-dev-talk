<?php
declare(strict_types=1);

namespace App\Strategies\Authentication;

use App\Contracts\AuthenticationStrategyContract;
use App\Strategies\Client\FacebookClient;
use Illuminate\Http\Request;

final class FacebookAuthentication implements AuthenticationStrategyContract
{
    public function login(Request $request)
    {
        $facebook = new FacebookClient($request);
        return $facebook->login();
    }
}
