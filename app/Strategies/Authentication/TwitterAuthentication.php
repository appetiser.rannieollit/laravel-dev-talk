<?php
declare(strict_types=1);

namespace App\Strategies\Authentication;

use App\Contracts\AuthenticationStrategyContract;
use App\Strategies\Client\TwitterClient;
use Illuminate\Http\Request;

final class TwitterAuthentication implements AuthenticationStrategyContract
{
    public function login(Request $request)
    {
        $twitterClient = new TwitterClient($request);
        return $twitterClient->login();
    }
}
