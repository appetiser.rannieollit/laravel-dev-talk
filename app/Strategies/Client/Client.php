<?php
declare(strict_types=1);

namespace App\Strategies\Client;

abstract class Client
{
    abstract public function login();
}
