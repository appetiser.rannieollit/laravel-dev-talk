<?php
declare(strict_types=1);

namespace App\Strategies\Client;

use Illuminate\Http\Request;

final class FacebookClient extends Client
{
    public function __construct(
        public Request $request
    ) {
    }

    public function login(): array
    {
        return [
          $this->getKeys(),
          $this->authorize(),
          'login with facebook'
        ];
    }

    private function getKeys(): string
    {
        return 'getKeys';
    }

    private function authorize(): string
    {
        return 'authorizing user';
    }
}
