<?php
declare(strict_types=1);

namespace App\Strategies\Client;

use Illuminate\Http\Request;

final class TwitterClient extends Client
{
    public function __construct(
        public Request $request
    ) {
    }

    public function login(): array
    {
        return ['login with twitter'];
    }
}
