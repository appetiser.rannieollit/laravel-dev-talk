<?php
declare(strict_types=1);

return [
    'driver' => env('EXPORT_DRIVER', 'json'),

    'json' => [

    ],

    'csv' => [

    ]
];
