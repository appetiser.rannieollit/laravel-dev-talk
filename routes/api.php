<?php

use App\Http\Controllers\API\FacebookLoginController;
use App\Http\Controllers\API\PostsController;
use App\Http\Controllers\API\TwitterLoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('/posts', PostsController::class);
Route::post('/post/export', [PostsController::class, 'export']);
Route::post('/facebook/login', [FacebookLoginController::class, 'login']);
Route::post('/twitter/login', [TwitterLoginController::class, 'login']);

